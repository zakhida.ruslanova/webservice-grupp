package com.example.demo.repository;

import com.example.demo.entity.PersonEntity;
import com.example.demo.entity.RoleEntity;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository< RoleEntity, Long> {
    RoleEntity findById(String id);
}
