package com.example.demo.repository;

import com.example.demo.entity.GruppEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GruppRepository extends CrudRepository< GruppEntity, String > {

    @Override
    List< GruppEntity > findAll();
}
