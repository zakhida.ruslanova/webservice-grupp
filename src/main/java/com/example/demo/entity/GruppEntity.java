package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "grupp")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GruppEntity {
    @Id
    String id;
    String gruppName;

    @ManyToMany(mappedBy = "grupper")
    List< PersonEntity > persons;
}

