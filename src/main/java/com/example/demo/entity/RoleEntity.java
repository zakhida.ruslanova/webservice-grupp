package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "role")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleEntity {
    @Id
    private String id;
    private String name;

    @JsonIgnore
    @ManyToMany(mappedBy = "roles")
    List< PersonEntity > persons;

    public void addPerson(PersonEntity person) {
        getPersons().add(person);
    }
}

