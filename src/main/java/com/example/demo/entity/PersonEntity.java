package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static javax.persistence.FetchType.EAGER;

@Entity
@Table(name = "person")
@Data
@NoArgsConstructor()
@AllArgsConstructor
public class PersonEntity {
    @Id
    String id;
    String firstName;
    String lastName;
    String username;
    String password;
    @ManyToMany(fetch = EAGER)
    private Collection< RoleEntity > roles = new ArrayList<>();

    @JsonIgnore
    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(name = "person_grupp", joinColumns = {@JoinColumn(name = "person_id")}, inverseJoinColumns = {
            @JoinColumn(name = "grupp_id")})
    List< GruppEntity > grupper;

    public void addRole(RoleEntity roleEntity) {
        getRoles().add(roleEntity);
    }

}
