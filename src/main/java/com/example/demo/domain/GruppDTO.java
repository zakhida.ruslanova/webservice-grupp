package com.example.demo.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@Value
public class GruppDTO {
    private String id;
    private String gruppName;
    private List< String > persons;

    @JsonCreator
    public GruppDTO(@JsonProperty("id") String id,
                    @JsonProperty("gruppName") String gruppName,
                    @JsonProperty("persons") List< String > persons) {
        this.id = id;
        this.gruppName = gruppName;
        this.persons = persons;
    }
}
