package com.example.demo.remote;

import com.example.demo.entity.PersonEntity;
import com.example.demo.exceptions.GroupClientException;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class RemotePerson {

    public Mono< PersonEntity > getPersonById(String personId) throws GroupClientException {
        try {
            WebClient webClient = WebClient.create("http://localhost:" + 8080 + "/persons/getPersonById/" + personId);

            return webClient.get()
                    .retrieve()
                    .bodyToMono(PersonEntity.class);
        } catch (Exception e) {
            throw new GroupClientException(e);
        }
    }
}

