package com.example.demo.remote;

import com.example.demo.entity.GruppEntity;
import com.example.demo.exceptions.GroupClientException;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class RemoteGroup {

    public Mono< List< GruppEntity > > getGroupById(String groupId) throws GroupClientException {
        try {
            WebClient webClient = WebClient.create("http://localhost:" + 8081 + "/groups/" + groupId);

            return webClient.get()
                    .retrieve()
                    .bodyToMono(new ParameterizedTypeReference< List< GruppEntity > >() {
                    });
        } catch (Exception e) {
            throw new GroupClientException(e);
        }
    }
}
