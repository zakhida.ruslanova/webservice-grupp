package com.example.demo.exceptions;

public class GroupClientException extends Exception {
    public GroupClientException(Throwable cause) {
        super(cause);
    }

}
