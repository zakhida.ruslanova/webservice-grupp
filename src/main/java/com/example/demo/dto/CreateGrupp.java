package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class CreateGrupp {
    String id;
    String gruppName;

    @JsonCreator
    public CreateGrupp(@JsonProperty String id,
                       @JsonProperty String gruppName) {
        this.id = id;
        this.gruppName = gruppName;
    }
}
