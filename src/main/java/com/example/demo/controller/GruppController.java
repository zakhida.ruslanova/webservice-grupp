package com.example.demo.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.demo.domain.GruppDTO;
import com.example.demo.dto.CreateGrupp;
import com.example.demo.entity.GruppEntity;
import com.example.demo.entity.PersonEntity;
import com.example.demo.entity.RoleEntity;
import com.example.demo.entityToDto.GruppEntityToDto;
import com.example.demo.exceptions.GroupClientException;
import com.example.demo.exceptions.GruppException;
import com.example.demo.repository.GruppRepository;
import com.example.demo.repository.PersonRepository;
import com.example.demo.service.GruppService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;

@RestController()
@RequestMapping("/grupper")
@RequiredArgsConstructor
public class GruppController implements GruppEntityToDto {

    private final GruppRepository gruppRepository;
    private final GruppService gruppService;
    private final PersonRepository personRepository;

    @GetMapping("/getAll")
    public List< GruppDTO > getGrupper() {
        return gruppService.getAllGrupper().map(this::GruppEntityToDto).collect(Collectors.toList());
    }

    @PostMapping("/createGrupp")
    public GruppDTO createGrupp(@RequestBody CreateGrupp createGrupp) {
        return GruppEntityToDto(gruppService.createGrupp(createGrupp));
    }

    @GetMapping("/getGruppById/{id}")
    public GruppDTO getGruppById(@PathVariable String id) throws GruppException {
        return GruppEntityToDto(gruppService.getGruppById(id));
    }

    @PutMapping(path = "/update/{gruppId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GruppDTO updateGruppById(@PathVariable String gruppId, @RequestBody CreateGrupp createGrupp) {
        GruppEntity foundGrupp = gruppRepository.findById(gruppId).get();
        foundGrupp.setGruppName(createGrupp.getGruppName());
        gruppRepository.save(foundGrupp);
        return GruppEntityToDto(foundGrupp);
    }

    @DeleteMapping("/delete/{gruppId}")
    public void deleteGrupp(@PathVariable String gruppId) {
        gruppRepository.findById(gruppId);
        gruppRepository.deleteById(gruppId);
    }

    @PostMapping("/role/save")
    public ResponseEntity< RoleEntity > saveRole(@RequestBody RoleEntity roleEntity) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/role/save").toUriString());
        return ResponseEntity.created(uri).body(gruppService.saveRole(roleEntity));
    }

    @PostMapping("/role/addToPerson/{personId}/{roleId}")
    public ResponseEntity< PersonEntity > addRoleToPerson(@PathVariable String personId, @PathVariable String roleId) throws GroupClientException {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/role/addToPerson/{personId}/{roleId}").toUriString());
        return ResponseEntity.created(uri).body(gruppService.addRoleToUser(personId, roleId));
    }

    @GetMapping("/token/refresh")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            try {
                String refresh_token = authorizationHeader.substring("Bearer ".length());
                Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = verifier.verify(refresh_token);
                String username = decodedJWT.getSubject();
                PersonEntity user = personRepository.findByUsername(username);
                String access_token = JWT.create()
                        .withSubject(user.getFirstName())
                        .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 60 * 1000))
                        .withIssuer(request.getRequestURI().toString())
                        .withClaim("roles", user.getRoles().stream().map(RoleEntity::getName).collect(Collectors.toList()))
                        .sign(algorithm);
                Map< String, String > tokens = new HashMap<>();
                tokens.put("access_token", access_token);
                tokens.put("refresh_token", refresh_token);
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), tokens);

            } catch (Exception exception) {
                response.setHeader("error", exception.getMessage());
                response.setStatus(FORBIDDEN.value());
                Map< String, String > error = new HashMap<>();
                error.put("error_message", exception.getMessage());
                response.setContentType(MimeTypeUtils.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), error);
            }
        } else {
            throw new RuntimeException("Refresh token is missing");
        }
    }
}

@Data
class RoleToUserForm {
    private String personId;
    private String roleId;
}

