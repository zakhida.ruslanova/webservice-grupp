package com.example.demo;

import com.example.demo.entity.GruppEntity;
import com.example.demo.entity.PersonEntity;
import com.example.demo.entity.RoleEntity;
import com.example.demo.repository.GruppRepository;
import com.example.demo.repository.PersonRepository;
import com.example.demo.service.GruppService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class Demo1Application {

    public static void main(String[] args) { SpringApplication.run(Demo1Application.class, args);}

    @Bean
    public PasswordEncoder passwordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }
    @Bean
    public CommandLineRunner createPersons(PersonRepository personRepository) {
        return args -> {
            personRepository.save(new PersonEntity("1", "Arnold", "Arny", "arny", "1234", new ArrayList<>(), new ArrayList<>()));
        };
    }

    @Bean
    public CommandLineRunner createGrupper(GruppRepository repoGrupp) {
        return args -> {
            repoGrupp.save(new GruppEntity("11", "Freedom", new ArrayList<>()));
            repoGrupp.save(new GruppEntity("12", "Peace", new ArrayList<>()));
            repoGrupp.save(new GruppEntity("13", "Hope", new ArrayList<>()));
            repoGrupp.save(new GruppEntity("14", "Grace", new ArrayList<>()));
            repoGrupp.save(new GruppEntity("15", "Smile", new ArrayList<>()));
        };
    }

    @Bean
    public CommandLineRunner demoData1(GruppService gruppService) {
        return args -> {
            gruppService.saveRole(new RoleEntity("100", "USER", new ArrayList<>()));
            gruppService.saveRole(new RoleEntity("200", "ADMIN", new ArrayList<>()));
            gruppService.addRoleToUser("1", "100");
        };
    }
}
