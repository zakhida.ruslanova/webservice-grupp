package com.example.demo.service;

import com.example.demo.dto.CreateGrupp;
import com.example.demo.entity.GruppEntity;
import com.example.demo.entity.PersonEntity;
import com.example.demo.entity.RoleEntity;
import com.example.demo.exceptions.GroupClientException;
import com.example.demo.exceptions.GruppException;
import com.example.demo.remote.RemotePerson;
import com.example.demo.repository.GruppRepository;
import com.example.demo.repository.PersonRepository;
import com.example.demo.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class GruppServiceImpl implements GruppService, UserDetailsService {

    @Autowired
    GruppRepository gruppRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    RemotePerson remotePerson;
    @Autowired
    PersonRepository personRepository;

    public GruppServiceImpl(GruppRepository gruppRepository, RoleRepository roleRepository, RemotePerson remotePerson, PersonRepository personRepository) {
        this.gruppRepository = gruppRepository;
        this.roleRepository = roleRepository;
        this.remotePerson = remotePerson;
        this.personRepository = personRepository;
    }

    @Override
    public Stream< GruppEntity > getAllGrupper() {
        return gruppRepository.findAll().stream();
    }

    @Override
    public GruppEntity createGrupp(CreateGrupp createGrupp) {
        return gruppRepository.save(new GruppEntity(
                createGrupp.getId(),
                createGrupp.getGruppName(),
                new ArrayList<>()));
    }

    @Override
    public GruppEntity getGruppById(String id) throws GruppException {
        return gruppRepository.findById(id).orElseThrow(() -> new GruppException("Grupp id " + id + " not found"));
    }

    @Override
    public RoleEntity saveRole(RoleEntity roleEntity) {
        log.info("Saving new role {} to the database", roleEntity.getName());
        return roleRepository.save(roleEntity);
    }

    @Override
    public PersonEntity addRoleToUser(String personId, String roleId) throws GroupClientException {
        log.info("Adding role {} to user {}", roleId, personId);
        PersonEntity person = remotePerson.getPersonById(personId).block();
        RoleEntity role = roleRepository.findById(roleId);
        person.addRole(role);
        role.addPerson(person);
        personRepository.save(person);
        roleRepository.save(role);
        return person;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        PersonEntity user = personRepository.findByUsername(username);
        if (user == null) {
            log.error("User not found in the database");
            throw new UsernameNotFoundException("User not found in the database");
        } else {
            log.info("User found in the database: {}", username);
        }
        Collection< SimpleGrantedAuthority > authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
    }
}
