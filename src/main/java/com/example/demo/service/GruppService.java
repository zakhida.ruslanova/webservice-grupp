package com.example.demo.service;

import com.example.demo.dto.CreateGrupp;
import com.example.demo.entity.GruppEntity;
import com.example.demo.entity.PersonEntity;
import com.example.demo.entity.RoleEntity;
import com.example.demo.exceptions.GroupClientException;
import com.example.demo.exceptions.GruppException;

import java.util.stream.Stream;

public interface GruppService {

    Stream< GruppEntity > getAllGrupper();

    GruppEntity createGrupp(CreateGrupp createGrupp);

    GruppEntity getGruppById(String id) throws GruppException;

    RoleEntity saveRole(RoleEntity roleEntity);

    PersonEntity addRoleToUser(String username, String rolename) throws GroupClientException;
}
