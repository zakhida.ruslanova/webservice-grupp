package com.example.demo.entityToDto;

import com.example.demo.domain.GruppDTO;
import com.example.demo.entity.GruppEntity;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public interface GruppEntityToDto {
    default GruppDTO GruppEntityToDto(GruppEntity gruppEntity) {
        return new GruppDTO(
                gruppEntity.getId(),
                gruppEntity.getGruppName(),
                gruppEntity.getPersons().stream().map(personEntity -> personEntity.getId())
                        .collect(Collectors.toList()));
    }
}
